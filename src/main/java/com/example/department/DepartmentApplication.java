package com.example.department;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class DepartmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DepartmentApplication.class, args);
	}

//	@Bean
//	CommandLineRunner init(DepartmentRepository departmentRepository) {
//		return args -> {
//			Stream.of("John", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
//				Department user = new Department(name, name.toLowerCase() + "@domain.com");
//				departmentRepository.save(user);
//			});
//			departmentRepository.findAll().forEach(System.out::println);
//		};
//	}
}
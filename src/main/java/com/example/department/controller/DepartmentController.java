package com.example.department.controller;

import com.example.department.domain.Department;
import com.example.department.repository.DepartmentRepository;
import com.example.department.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://ngopenshift-test-app.apps.vrls2.5c0f.sandbox1183.opentlc.com")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/department")
    public List<Department> getDepartments() {
        return departmentService.findAll();
    }

    @GetMapping("/department/{id}")
    public Department getDepartment(@PathVariable("id") Long id) {
        return departmentService.findById(id);
    }


    @PostMapping("/department/{id}")
    void saveDepartment(@RequestBody Department department) {
        departmentService.save(department);
    }


}
package com.example.department.kafka.util;

import com.example.department.kafka.dto.Greeting;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class EventDeserializer implements Deserializer<Object> {

    @Override
    public Object deserialize(String topic, byte[] data){
        Object event = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            event = objectMapper.readValue(data, Greeting.class);
        }catch(IOException ioe){

        }

        return event;
    }
}

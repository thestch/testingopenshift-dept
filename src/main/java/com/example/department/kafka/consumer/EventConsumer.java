package com.example.department.kafka.consumer;

import com.example.department.kafka.dto.Greeting;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.apache.kafka.clients.consumer.ConsumerRecord;

@Component
public class EventConsumer {

    private static final String TOPIC = "my-topic";

    //@KafkaListener(topics = TOPIC)
    public void messageListener(ConsumerRecord<String, Greeting> message){
        System.out.println("Name" + message.value().getName() + " - " + "Message" + message.value().getMessage());
    }
}

package com.example.department.service;

import com.example.department.domain.Department;
import com.example.department.repository.DepartmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentService {

    Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private DepartmentRepository departmentRepository;

    @PostConstruct
    public void init(){
        Department department1 = new Department("IT department", "it.department@domain.com");
        Department department2 = new Department("Finance department", "finance.department@domain.com");

        departmentRepository.save(department1);
        departmentRepository.save(department2);
    }

    public Department findById(Long id){
        return departmentRepository.findById(id).orElse(null);
    }

    public List<Department> findAll(){
        List<Department> departments = new ArrayList<>();
        departmentRepository.findAll().forEach(departments::add);

        return departments;
    }

    public void save(Department department){
        departmentRepository.save(department);
    }
}

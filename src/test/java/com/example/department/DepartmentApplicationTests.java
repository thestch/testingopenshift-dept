package com.example.department;

import com.example.department.kafka.consumer.EventConsumer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


@SpringBootTest
@MockBean({
		EventConsumer.class
})
class DepartmentApplicationTests {

	@Test
	void contextLoads() {
	}

}
